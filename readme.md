# JavaScript Embed Scaffolding

A basic framework for distributing and rendering embeddable third-party widgets:

* The amount of code to embed is absolutely minimal
* You can include multiple widgets in a single page
* Widgets are rendered as iFrames
* Supports Google Analytics

Do you want hot embeds that look like this?

```html
<script src="//example.com/load.js?id=12345&type=foo" async></script>
```

Sure you do.

### How it works

Pretty much everything is accomplished by two files: 

* ```embed/js/load.js``` is the "bootstrap" file that end users link to when they embed your widget in their page. You can include as many (or as few) query string parameters as you like for things like options, settings, and user IDs.
* ```embed/js/view.js``` gets injected into the end user's page (once) by ```load.js``` and renders the iFrame widgets according to your specifications.

### Installation

First things first, ```embed/js/load.js``` is the file that end users will embed on their page. Let's configure a few settings at the top of the script:

```javascript
baseUrl = '//example.com/assets/embed/' // absolute path to where the embed assets reside
loadUrl  = baseUrl + 'js/load.js' // path to the "load.js" script (i.e. this file)
embedUrl = baseUrl + 'js/view.js' // path to the "view.js" script
```

Then go into ```embed/js/view.js``` to customize the ```renderEmbed()``` function based on your embeds:

* Parse any query string parameters passed in via the original embed code
* Set the styles for the embedded iFrame
* Set the source URL for the iFrame

### Analytics

Even though we're embedding our widget as third-party content, we can track analytics for individual (anonymous) users using a combination of cookies and localStorage.

Within the widgets themselves, include ```embed/js/analytics.js``` and then instantiate Google Analytics like so:

```javascript
    ga('create', 'UA-XXXXX-Y', {
	  'storage': 'none',
	  'clientId': getPersistentVisitorId() // defined in analytics.js
	});
```

Check out any of the widget HTML files in ```embed/widgets``` to see how it's set up.